const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const devMode = process.env.NODE_ENV !== 'production'

const front = {
	target: 'web',
  entry: './src/front/main.js',
  output: {
		path: path.resolve(__dirname, './dist/static/content'),
		filename: 'js/front.bundle.js',
		publicPath: './dist/static/content/'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components|back)/,
        loader: 'babel-loader',
        query: {
          presets: ['env', 'es2015'],
        },
      },
      {
        test: /\.s?[ac]ss$/,
        use: [
					'css-hot-loader',
					!devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
					'css-loader',
					'sass-loader',
        ],
      },
    ],
  },
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
    new MiniCssExtractPlugin({
			filename: devMode ? 'css/[name].css' : 'css/[name].[hash].css',
			chunkFilename: devMode ? 'css/[id].css' : 'css/[id].[hash].css'
		})
	],
  resolve: {
    extensions: ['*', '.js'],
  },
  devServer: {
		contentBase: './dist/static/content',
		publicPath: '/',
    compress: true,
		watchContentBase: true,
		port: 8000,
		proxy: {
			'/': {
				target: 'http://localhost:8083',
				pathRewrite: { '^/api': '' },
				secure: false,
			}
		}
	},
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
  },
  stats: {
    colors: true,
  },
  devtool: 'source-map',
};

module.exports = [front];
