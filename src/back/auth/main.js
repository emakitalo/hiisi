import {authenticationMiddleware} from './middleware';
import session from 'express-session';
import {rdb, getUsers} from '../rethink';
const RedisStore = require('connect-redis')(session);
const passport = require('passport');
const bcrypt = require('bcryptjs');
const LocalStrategy = require('passport-local').Strategy;

function findUser(username, callback) {
	getUsers(['id', 'groups', 'password']).then((users) => {
		if (Object.keys(users).includes(username)) {
			const userData = users[username];
			const user = {
				username: username,
				password: userData.password,
				id: userData.id,
			};
			return callback(null, user);
		}
		return callback(null);
	})
}

passport.serializeUser(function(user, cb) {
  cb(null, user.username);
});

passport.deserializeUser(function(username, cb) {
  findUser(username, cb);
});

export function initPassport(app) {
  passport.use(
    new LocalStrategy((username, password, done) => {
      findUser(username, (err, user) => {
        if (err) {
          return done(err);
        }

        // User not found
        if (!user) {
          return done(null, false);
        }

        // Always use hashed passwords and fixed time comparison
        bcrypt.compare(password, user.password, (err, isValid) => {
          if (err) {
            return done(err);
          }
          if (!isValid) {
            return done(null, false);
          }
          return done(null, user);
        });
      });
    }),
  );

  passport.authenticationMiddleware = authenticationMiddleware;

  app.use(
    session({
      store: new RedisStore({
        url: process.env.REDIS_STORE_URI,
        cookie: {secure: false, maxAge: 86400000},
      }),
      secret: process.env.REDIS_STORE_SECRET,
      resave: false,
      saveUninitialized: false,
    }),
  );

  app.use(passport.initialize());
  app.use(passport.session());
}
