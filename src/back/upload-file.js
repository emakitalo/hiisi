const fileUpload = require('express-fileupload');
const fs = require('fs');

export function initUpload(app, staticPath, auth) {
	app.use(fileUpload());

  app.post('/upload', auth(), (req, res) => {
    if (Object.keys(req.files).length == 0) {
      return res.status(400).send('No files were uploaded.');
    }

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
		const files = [];
    for (let [fullPath, file] of Object.entries(req.files)) {
      // Use the mv() method to place the file somewhere on your server
			let dirs = fullPath.split('/');
			if(req.user.username === dirs[0]) {
				dirs.pop();
				let path = '';
				for(let i=0; i < dirs.length; i++) {
					path += dirs[i] + '/';
					if(!fs.existsSync(staticPath + path)){
						fs.mkdirSync(staticPath + path);
					}
				}
				file.mv(staticPath + path + file.name, function(err) {
					if (err) return res.status(500).send(err);
				});
				files.push(file.name);
			}
    }

		res.send(JSON.stringify({
			message: 'Files uploaded!',
			files: files
		}));
  });
}
