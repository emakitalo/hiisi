export const base = './dist/static/';
export const published = base + 'public/';
export const unpublished = base + 'private/';
export const content = base + 'content/';
export const subaddr = '';
