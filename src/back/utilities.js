const Promise = require('bluebird');
import path from 'path';
import * as PATHS from './paths';
const fs = require('fs').promises;
const fsC = require('fs').constants; 
const rimraf = Promise.promisify(require('rimraf'));
const mv = Promise.promisify(require('mv'));

export function listFolder(folder){
	return fs.readdir(folder).then(folders => {
		return Promise.all(folders.map((file) => {
			return fs.stat(path.resolve(folder, file)).then((stat) => {
				if(stat.isDirectory()) {
					return listFolder(path.resolve(folder, file)).then(value => { return {[file]: value}; });
				} else {
					return file;
				}
			});
		}));
	}).then((results) => {
		return Array.prototype.concat.apply([], results);
	});
}

export function renderMenu(data, path = '', depth = 1) {
	return Promise.all(data.map(item => {
		return new Promise((res, rej) => {
			if(typeof item === 'object') {
				const key = Object.keys(item)[0];
				const newItem = item[key];
				if(newItem.includes('index.html')) {
					res('<li class="' + key + '"><a href="' + path + key + '/index.html">' + key + '</a></li>');
				} else {
					if(key === 'static-files' && depth === 1) {
						res('');
					} else {
						renderMenu(newItem, path + key + '/', depth + 1)
						.then(results => {
							res(
								'<li><p class="folder ' + key.split('.').slice(-1) + '">' + key + 
								'<a class="nav-goto" href="' + path + key + '"></a></p>' +
								'<ul>' + results.join('') + '</ul></li>');
						});
					}
				}
			} else {
				res('<li><a href="' + path + item + '">' + item + '</a></li>');
			}
		});
	}))
}

export function symlinkFolder(oldPath, folder){
	return fs.readdir(folder).then(folders => {
		return Promise.all(folders.map((file) => {
			return fs.stat(path.resolve(folder + '/' + file)).then((stat) => {
				if(stat.isDirectory()) {
					const newPath = oldPath + '/' + file;
					return fs.mkdir(PATHS.published + newPath).then(() => {
						return symlinkFolder(newPath, path.join(folder, file));
					})
					.catch((err) => {
						console.log(err);
					});
				} else {
					return fs.symlink(
						path.resolve(PATHS.unpublished + oldPath),
						PATHS.published + oldPath + '/' + file
					)
					.then(() => {})
					.catch((err) => {
						console.log(err);
					});
				}
			})
			.catch((err) => {
				return fs.symlink(
					path.resolve(PATHS.unpublished + oldPath + '/' + file),
					PATHS.published + oldPath + '/' + file
				)
				.then(() => {})
				.catch((err) => {
					console.log(err);
				});
			});
		}));
	})
	.then((results) => {
		return Array.prototype.concat.apply([], results);
	});
};

export function renameFromJSON(staticPath, linkPath, json) {
	return Promise.all(
		json.map(value => {
			if(typeof value === 'object') {
				Object.keys(value).map(key => {
					renameFromJSON(staticPath, key, value[key]).then(results => {
						return Promise.resolve('FINISHED');
					});
				})
			} else {
				return new Promise((res, rej) => {
					fs.stat(staticPath + value).then((stat) => { 
						fs.access(staticPath + value, fsC.F_OK).then(() => {
							fs.access(PATHS.published + value, fsC.F_OK).then(() => { 
								mv(staticPath + value, staticPath + linkPath).then(() => {
									const pubPath = stat.isDirectory() ? 
										linkPath : linkPath.split('/').slice(0,-1).join('/');
									fs.access(PATHS.published + pubPath, fsC.F_OK).then(() => { 
										rimraf(PATHS.published + value).then(() => { 
											fs.symlink(
												path.resolve(PATHS.unpublished + linkPath),
												PATHS.published + linkPath)
											.then(() => { res('FINISHED'); })
										})
										.catch(err => { console.log(err) })
									})
									.catch((err) => {
										let currentPath = '';
										let newDir = '';
										let folders = stat.isDirectory() ? 
											linkPath.split('/') : linkPath.split('/').slice(0,-1);

										fs.mkdir(PATHS.published + folders.join('/'), {recursive: true}).then(() => {
											if(stat.isDirectory()) {
												symlinkFolder(linkPath, PATHS.published + value).then((values) => {
													rimraf(PATHS.published + value).then(() => { res('FINISHED'); })
													.catch((err) => { console.log(err); });
												})
												.catch((err) => { console.log(err); });
											} else {
												fs.symlink(
													path.resolve(PATHS.unpublished + linkPath),
													PATHS.published + linkPath)
												.then(() => {
													rimraf(PATHS.published + value).then(() => { res('FINISHED'); })
													.catch((err) => { console.log(err); });
												})
												.catch((err) => { console.log(err); });
											}
										})
										.catch((err) => { console.log(err); });
									});
								})
								.catch((err) => { console.log(err); });
							})
							.catch((err) => {
								mv(staticPath + value, staticPath + linkPath).then(() => { res('FINISHED'); })
								.catch((err) => { console.log(err); });
							});
						})
						.catch((err) => { console.log(err) });
					})
					.catch((err) => { console.log(err); })
				})
			}
		})
	)
	.then(results => {
		return Promise.resolve('ALL FINISHED');
	})
}

export function createSubDirs(path, loc, func = () => {}) {
	loc += path.pop() + '/';
	fs.access(loc, fsC.F_OK).then(() => {
		if(path.length > 0) {
			createSubDirs(path, loc, func);
		} else {
			func();
		}
	})
	.catch((err) => {
		fs.mkdir(loc).then(() => { 
			if(path.length > 0) {
				createSubDirs(path, loc, func);
			} else {
				func();
			}
		})
		.catch((err) => { console.log(err); });
	});
}
