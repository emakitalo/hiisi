const Promise = require("bluebird");
import passport from "passport";
import * as Utils from "../utilities";
import * as fileUpload from "express-fileupload";
import express from "express";
import path from "path";
import * as PATHS from "../paths";
import * as RDB from "../rethink";
import { folderContents } from "../utilities";
import exphbs from "express-handlebars";
import { mimeTypes } from "../mime-types";
import Markdown from "markdown-it";
import { html5Media } from "markdown-it-html5-media";

const rimraf = Promise.promisify(require("rimraf"));
const fs = require("fs-extra");
const createReadStream = require("fs").createReadStream;
const fsC = require("fs").constants;
const bcrypt = require("bcryptjs");
const cors = require("cors");

const api = express.Router();
const rdb = RDB.rdb;

const templatePath = process.cwd() + "/src/back/";
const hbs = exphbs.create({
  defaultLayout: "layout",
  extname: ".hbs",
  layoutsDir: templatePath,
  partialsDir: [templatePath],
  helpers: {}
});

const md = new Markdown({
  html: true,
  linkify: true,
  typographer: true
});

md.use(html5Media);

function routeUser(app, user) {
  hbs.partialsDir.push(
    path.resolve(PATHS.unpublished + user + "/static-files/handlebars/")
  );
  app.get("/" + user, (req, res) => {
    app.set(
      "views",
      path.resolve(PATHS.unpublished + user + "/static-files/handlebars/")
    );
    RDB.getUsers(["published"]).then(users => {
      Utils.renderMenu(users[user].published, PATHS.subaddr + user + "/").then(
        results => {
          res.render(
            "main",
            {
              layout: false,
              published: results.join(""),
              title: user,
              user: user,
              location: "/"
            },
            (err, html) => {
              if (err) {
                app.set("views", templatePath);
                res.render(
                  "default-user",
                  {
                    layout: false,
                    published: results.join(""),
                    user: user,
                    title: user
                  },
                  (err, html) => {
                    if (err) res.send("User " + user + " not found");
                    else res.send(html);
                  }
                );
              } else {
                res.send(html);
              }
              app.set("views", templatePath);
            }
          );
        }
      );
    });
  });

  app.get("/" + user + "/*", (req, res) => {
    app.set("views", templatePath);
    const fPath = PATHS.published + user + "/" + req.params["0"];

    fs.stat(fPath)
      .then(stat => {
        if (!stat.isDirectory()) {
          const ext = req.params["0"].split(".").slice(-1)[0];
          if (ext !== "md") {
            if (mimeTypes[ext]) {
              res.writeHead(200, {
                "Content-Type": mimeTypes[ext],
                "Content-Length": stat.size
              });
            }
            createReadStream(fPath).pipe(res);
          } else {
            fs.readFile(fPath, "utf-8").then(body => {
              let folder = fPath.split("/");
              folder.splice(-1, 1);
              folder = folder.join("/");
              fs.readdir(folder)
                .then(files => {
                  let nav = "";
                  let linkFolder = req.params["0"].split("/");
                  let fileName = linkFolder.pop().split(".")[0];
                  linkFolder = linkFolder.join("/");
                  files.map(file => {
                    let fName = file.split(".");
                    if (fName.pop() === "md")
                      nav +=
                        '<li><a href="' +
                        PATHS.subaddr +
                        user +
                        "/" +
                        linkFolder +
                        "/" +
                        file +
                        '">' +
                        fName +
                        "</a></li>";
                  });
                  app.set(
                    "views",
                    path.resolve(
                      PATHS.unpublished + user + "/static-files/handlebars/"
                    )
                  );
                  res.render("post", {
                    layout: false,
                    body: md.render(body),
                    nav: nav,
                    title: fileName
                  });
                })
                .catch(err => {
                  console.log(err);
                });
            });
          }
        } else {
          const dPath = PATHS.unpublished + user + "/" + req.params["0"];
          fs.stat(dPath + "/.hbs")
            .then(stat => {
              const template = req.params["0"].split("/").join("#");
              app.set(
                "views",
                path.resolve(
                  PATHS.unpublished + user + "/static-files/handlebars/"
                )
              );
              res.render(template, { user: user });
            })
            .catch(err => {
              rdb
                .table("users")
                .filter(rdb.row("name").eq(user))
                .then(data => {
                  let published = data[0].published;
                  const path = req.params["0"].split("/");
                  path.map(folder => {
                    published.map(item => {
                      if (typeof item === "object") {
                        if (item[folder]) published = item[folder];
                      }
                    });
                  });
                  Utils.renderMenu(
                    published,
                    PATHS.subaddr + user + "/" + req.params["0"] + "/"
                  )
                    .then(result => {
                      let data = result.join("");
                      let userData = "<ul>" + data + "</ul>";
                      res.render("main", {
                        published: userData,
                        user: user,
                        layout: false,
                        location:
                          PATHS.subaddr +
                          user +
                          "/" +
                          req.params["0"].split("/").slice(0, -1).join("/")
                      });
                    })
                    .catch(err => {
                      console.log(err);
                    });
                });
            });
        }
      })
      .catch(err => {
        res.send(err);
      });
  });
}

export function initManager(app) {
  app.use(cors());
  app.set("trust proxy", "loopback, 127.0.0.1");

  app.engine(".hbs", hbs.engine);

  app.set("view engine", ".hbs");
  app.set("views", templatePath);

  app.use(
    "/static/private",
    passport.authenticationMiddleware(),
    express.static(PATHS.unpublished)
  );

  app.use("/static/public", express.static(PATHS.published));

  app.use("/static/content", express.static(PATHS.content));

  app.use(fileUpload());

  RDB.initDB();

  app.get("/", renderMain);
  app.get("/admin", passport.authenticationMiddleware(), renderWelcome);
  app.get("/login", renderLogin);

  RDB.getUsers(["published"]).then(users => {
    Object.keys(users).map(user => {
      routeUser(app, user);
    });
  });

  app.post(
    "/login",
    passport.authenticate("local", {
      successRedirect: PATHS.subaddr + "/admin",
      failureRedirect: PATHS.subaddr + "/login"
    })
  );

  app.get("/logout", (req, res) => {
    req.logout();
    res.redirect("/");
  });

  api.use((req, res, next) => {
    next();
  });

  api.post("/create-user", passport.authenticationMiddleware(), (req, res) => {
    const data = req.body;
    rdb
      .table("users")
      .filter(rdb.row("name"))
      .eq(data.name)
      .run()
      .then(user => {
        res.send(data.name + " already exists!");
      })
      .catch(err => {
        const newUser = {
          name: data.name,
          password: data.password,
          groups: JSON.parse(data.groups),
          media: [],
          published: []
        };
        RDB.createUserRDB(newUser, () => {
          routeUser(app, data.name);
          res.json({ [newUser.name]: { groups: newUser.groups } });
        });
      });
  });

  api.put("/update-user", passport.authenticationMiddleware(), (req, res) => {
    const data = req.body;
    rdb
      .table("users")
      .filter(rdb.row("name").eq(data.name))
      .run()
      .then(user => {
        user = user[0];
        if (req.user.username === user.name || user.groups.includes("admin")) {
          bcrypt.compare(data.password, user.password, (err, isValid) => {
            if (err) {
              return err;
            } else if (!isValid) {
              return isValid;
            } else {
              RDB.updateUser(data, () => {
                req.logout();
                res.send("Login with your new password");
              });
            }
          });
        }
      })
      .catch(err => {
        res.send(data.name + " already exists!");
      });
  });

  api.post("/upload", passport.authenticationMiddleware(), (req, res) => {
    if (Object.keys(req.files).length == 0) {
      return res.status(400).send("No files were uploaded.");
    }
    const files = [];
    const promises_1 = [];
    for (let [fullPath, file] of Object.entries(req.files)) {
      promises_1.push(
        new Promise((res, rej) => {
          let dirs = fullPath.split("//").join("/").split("/");
          if (req.user.username === dirs[0]) {
            dirs.pop();
            let path = "";
            const promises_2 = [];
            for (let i = 0; i < dirs.length; i++) {
              path += dirs[i] + "/";
              promises_2.push(
                new Promise((res, rej) => {
                  fs.access(PATHS.unpublished + path, fsC.F_OK)
                    .then(() => {
                      res("");
                    })
                    .catch(err => {
                      fs.mkdir(PATHS.unpublished + path, { recursive: true })
                        .then(() => {
                          res("");
                        })
                        .catch(err => {
                          console.log(err);
                        });
                    });
                })
              );
            }

            Promise.all(promises_2).then(val => {
              file
                .mv(PATHS.unpublished + fullPath)
                .then(() => {
                  res("");
                })
                .catch(err => {
                  return res.status(500).send(err);
                });
            });
          }
        })
      );
    }

    Promise.all(promises_1)
      .then(results => {
        RDB.updateUserMedia("media", req.user.username, () => {
          const folderPath = Object.keys(req.files)[0].split("//");
          const folderName = folderPath[1].split("/")[0];
          Utils.listFolder(
            PATHS.unpublished +
              folderPath[0] +
              (folderName.includes(".") ? "" : "/" + folderName)
          ).then(folders => {
            res.send(
              JSON.stringify({
                message: "Files uploaded!",
                files: [{ [folderName]: folders }]
              })
            );
          });
        });
      })
      .catch(err => {
        console.log(err);
      });
  });

  api.get("/get-media", passport.authenticationMiddleware(), (req, res) => {
    rdb
      .table("users")
      .filter(rdb.row("name").eq(req.user.username))
      .then(data => {
        res.json({ media: data[0].media, published: data[0].published });
      });
  });

  api.get("/get-public-media/:username", (req, res) => {
    rdb
      .table("users")
      .filter(rdb.row("name").eq(req.params.username))
      .then(data => {
        res.json({ published: data[0].published });
      })
      .catch(err => {
        res.send("User not found!");
      });
  });

  api.get("/get-users", passport.authenticationMiddleware(), (req, res) => {
    RDB.getUsers(["groups"]).then(users => {
      res.json(users);
    });
  });

  api.post("/get-file", passport.authenticationMiddleware(), (req, res) => {
    fs.stat(PATHS.unpublished + req.body.file)
      .then(stat => {
        const ext = req.body.file.split(".").slice(-1)[0];
        if (mimeTypes[ext]) {
          res.writeHead(200, {
            "Content-Type": mimeTypes[ext],
            "Content-Length": stat.size
          });
        }
        createReadStream(PATHS.unpublished + req.body.file).pipe(res);
      })
      .catch(err => {
        console.log(err);
        res.json({ message: "Error file not found!", data: req.body.file });
      });
  });

  api.post("/save-file", passport.authenticationMiddleware(), (req, res) => {
    fs.writeFile(PATHS.unpublished + req.body.file, req.body.data)
      .then(() => {
        res.json({ message: "File was saved!" });
      })
      .catch(err => {
        res.json({ message: err });
      });
  });

  api.post("/remove-file", passport.authenticationMiddleware(), (req, res) => {
    fs.access(PATHS.published + req.body.file, fsC.F_OK)
      .then(() => {
        fs.unlink(PATHS.published + req.body.file)
          .then(() => {
            RDB.updateUserMedia("published", req.user.username);
            fs.access(PATHS.unpublished + req.body.file, fsC.F_OK)
              .then(() => {
                fs.unlink(PATHS.unpublished + req.body.file)
                  .then(() => {
                    RDB.updateUserMedia("media", req.user.username);
                    res.json({ message: "success", state: true });
                  })
                  .catch(err => {
                    res.json({ message: err, state: false });
                  });
              })
              .catch(err => {
                res.json({ message: err, state: false });
              });
          })
          .catch(err => {
            res.json({ message: err, state: false });
          });
      })
      .catch(err => {
        fs.access(PATHS.unpublished + req.body.file, fsC.F_OK).then(() => {
          fs.unlink(PATHS.unpublished + req.body.file)
            .then(() => {
              RDB.updateUserMedia("media", req.user.username);
              res.json({ message: "success", state: true });
            })
            .catch(err => {
              res.json({ message: err, state: false });
            });
        });
      });
  });

  api.post("/publish", passport.authenticationMiddleware(), (req, res) => {
    let fileName = req.body.file.slice(0, -1);

    fs.access(PATHS.unpublished + fileName, fsC.F_OK)
      .then(() => {
        fs.lstat(PATHS.unpublished + fileName)
          .then(stats => {
            fs.access(PATHS.published + fileName, fsC.F_OK)
              .then(() => {
                if (stats.isFile()) {
                  fs.unlink(PATHS.published + fileName)
                    .then(() => {
                      RDB.updateUserMedia("published", req.user.username);
                    })
                    .catch(err => {
                      console.log(err);
                    });
                } else {
                  rimraf(PATHS.published + fileName)
                    .then(() => {
                      RDB.updateUserMedia("published", req.user.username);
                    })
                    .catch(err => {
                      console.log(err);
                    });
                }
                res.json({ message: fileName, state: false });
              })
              .catch(err => {
                let link = fileName.split("/");
                const file = stats.isFile() ? link.pop() : undefined;
                const folders = link.slice().reverse();
                link = link.join("/");
                if (stats.isFile()) {
                  const createSymLink = () => {
                    fs.symlink(
                      path.resolve(PATHS.unpublished + fileName),
                      PATHS.published + link + "/" + file,
                      stats.isFile() ? "file" : "dir"
                    )
                      .then(() => {
                        RDB.updateUserMedia("published", req.user.username);
                      })
                      .then(err => {
                        console.log(err);
                      });
                  };

                  fs.access(PATHS.published + link)
                    .then(() => {
                      createSymLink();
                    })
                    .catch(err => {
                      Utils.createSubDirs(
                        folders,
                        PATHS.published,
                        createSymLink
                      );
                    });
                } else {
                  const createSymLink = () => {
                    fs.readdir(PATHS.unpublished + link)
                      .then(files => {
                        if (files.length > 0) {
                          const promises = [];
                          files.forEach(name => {
                            promises.push(
                              fs
                                .lstat(PATHS.unpublished + link + "/" + name)
                                .then(stats => {
                                  if (stats.isFile()) {
                                    fs.access(
                                      PATHS.published + link + "/" + name,
                                      fsC.F_OK
                                    )
                                      .then(() => {})
                                      .catch(err => {
                                        fs.symlink(
                                          path.resolve(
                                            PATHS.unpublished +
                                              link +
                                              "/" +
                                              name
                                          ),
                                          PATHS.published + link + "/" + name,
                                          stats.isFile() ? "file" : "dir"
                                        )
                                          .then(() => {})
                                          .catch(err => {
                                            console.log(err);
                                          });
                                      });
                                  }
                                })
                                .catch(err => {
                                  console.log(err);
                                })
                            );
                          });

                          Promise.all(promises).then(res => {
                            RDB.updateUserMedia("published", req.user.username);
                          });
                        } else {
                          RDB.updateUserMedia("published", req.user.username);
                        }
                      })
                      .catch(err => {
                        console.log(err);
                      });
                  };

                  fs.access(PATHS.published + link, fsC.F_OK)
                    .then(() => {
                      createSymLink();
                    })
                    .catch(err => {
                      Utils.createSubDirs(
                        folders,
                        PATHS.published,
                        createSymLink
                      );
                    });
                }
                res.json({ message: fileName, state: true });
              });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .catch(err => {
        res.json({ message: "Folder " + req.body.file + " does not exist." });
      });
  });

  api.post("/rename", passport.authenticationMiddleware(), (req, res) => {
    const files = JSON.parse(req.body["files"]);
    Promise.all(
      Object.keys(files).map(path => {
        if (req.body["duplicate"]) {
          return fs.copy(
            PATHS.unpublished + files[path],
            PATHS.unpublished + path
          );
        } else {
          return Utils.renameFromJSON(PATHS.unpublished, path, files[path]);
        }
      })
    )
      .then(values => {
        RDB.updateUserMedia("media", req.user.username, () => {
          RDB.updateUserMedia("published", req.user.username, () => {
            res.json({
              message: "all files processed",
              files: files,
              duplicate: req.body["duplicate"] ? true : false
            });
          });
        });
      })
      .catch(err => {
        res.send(err);
      });
  });

  api.post(
    "/remove-folder",
    passport.authenticationMiddleware(),
    (req, res) => {
      fs.access(PATHS.published + req.body.folder, fsC.F_OK)
        .then(() => {
          rimraf(PATHS.published + req.body.folder)
            .then(() => {
              RDB.updateUserMedia("published", req.user.username);
              fs.access(PATHS.unpublished + req.body.folder, fsC.F_OK)
                .then(() => {
                  rimraf(PATHS.unpublished + req.body.folder)
                    .then(() => {
                      RDB.updateUserMedia("media", req.user.username);
                      res.json({ message: req.body.folder, state: true });
                    })
                    .catch(err => {
                      console.log(err);
                    });
                })
                .catch(err => {
                  //res.json({'message': 'Folder ' + req.body.folder + ' does not exist.'});
                });
            })
            .catch(err => {
              console.log(err);
            });
        })
        .catch(err => {
          fs.access(PATHS.unpublished + req.body.folder, fsC.F_OK)
            .then(() => {
              rimraf(PATHS.unpublished + req.body.folder)
                .then(() => {
                  RDB.updateUserMedia("media", req.user.username);
                  res.json({ message: req.body.folder, state: true });
                })
                .catch(err => {
                  console.log(err);
                });
            })
            .catch(err => {
              res.json({
                message: "Folder " + req.body.folder + " does not exist."
              });
            });
        });
    }
  );

  api.post("/add-folder", passport.authenticationMiddleware(), (req, res) => {
    const fileOrFolder = req.body.folder ? req.body.folder : req.body.file;
    fs.access(PATHS.unpublished + fileOrFolder)
      .then(() => {
        res.json({ message: fileOrFolder + " already exists." });
      })
      .catch(err => {
        if (req.body.folder) {
          fs.mkdir(PATHS.unpublished + fileOrFolder)
            .then(() => {
              RDB.updateUserMedia("media", req.user.username);
              res.json({ message: fileOrFolder, state: true });
            })
            .catch(err => {
              console.log(err);
            });
        } else {
          fs.writeFile(PATHS.unpublished + fileOrFolder, "")
            .then(() => {
              RDB.updateUserMedia("media", req.user.username);
              res.json({ message: fileOrFolder, state: false });
            })
            .catch(err => {
              console.log(err);
            });
        }
      });
  });

  app.use("/", api);
}

function renderMain(req, res) {
  RDB.getUsers(["name", "published"]).then(users => {
    let published = "<ul>";
    const promises = [];
    Object.keys(users).map(name => {
      const user = users[name];
      promises.push(
        new Promise((res, rej) => {
          Utils.renderMenu(user.published, PATHS.subaddr + name + "/")
            .then(result => {
              let data = result.join("");
              let userData =
                '<li><p class="folder open">' +
                name +
                '<a class="nav-goto" href="' +
                name +
                '"></a>' +
                "</p>" +
                "<ul>" +
                data +
                "</ul></li>";
              res(userData);
            })
            .catch(err => {
              res.send(err);
            });
        })
      );
    });

    Promise.all(promises)
      .then(results => {
        published += results.join("") + "</ul>";
        res.render("main", {
          published: published,
          title: "LVL001",
          user: "test-user",
          location: "/"
        });
      })
      .catch(err => {
        res.send(err);
      });
  });
}

function renderLogin(req, res) {
  res.render("login");
}

function renderWelcome(req, res) {
  res.render("manager", { user: req.user });
}
