import express from 'express'; 
import {initPassport} from './auth/main.js';
import {initManager} from './manager/main.js';
import {renderMenu} from './utilities.js';
import path from 'path';
import bodyParser from 'body-parser';

const app = express();

app.use(bodyParser.urlencoded({
  extended: false
}));

initPassport(app);
initManager(app);

app.listen({ port: 8083, host: 'localhost'}, () => {
  console.log('Listening');
});

