import {listFolder, folderContents} from './utilities';
import * as PATHS from './paths'
const Promise = require('bluebird');
const fs = require('fs').promises;
const fsC = require('fs').constants;
const bcrypt = Promise.promisifyAll(require('bcryptjs'));

const db = 'lvl001_beta';
const userName = process.env.TINYCMS_USER;
const password = process.env.TINYCMS_PASSWORD;

export const rdb = require('rethinkdbdash')({
		host: 'localhost',
		port: 28015,
		db: db
});

export function getUsers(keys) {
	return rdb.table('users').run().then((res) => {
		const users = {};
		res.map((user) => {
			users[user.name] = {};
			keys.forEach(key => users[user.name][key] = user[key]);
		});
		return Promise.resolve(users);
	});
}

export function initDB() {
	rdb.dbCreate(db).run().then((res) =>  {
		console.log(JSON.stringify(res, null, 2));
	});

	rdb.db(db).tableCreate('users').run().then((res) => {
		console.log(JSON.stringify(res, null, 2));
	});

  fs.access(PATHS.published, fsC.F_OK).then(() => { 
    console.log("Path " + PATHS.published + " already exists!");
  })
  .catch((err) => {
    fs.mkdir(PATHS.published).then(() => { 
      console.log("Path " + PATHS.published + " created!");
    })
    .catch((err) => { console.log(err); });
  });

  fs.access(PATHS.unpublished, fsC.F_OK).then(() => { 
    console.log("Path " + PATHS.unpublished + " already exists!");
  })
  .catch((err) => {
    fs.mkdir(PATHS.unpublished).then(() => { 
      console.log("Path " + PATHS.unpublished + " created!");
    })
    .catch((err) => { console.log(err); });
  });

	rdb.table('users')
	.filter(rdb.row('name').eq(userName))
	.run().then((res) => {
		if(res.length < 1) {
			createUserRDB(
				{
					name: userName,
					password: password,
					groups: ['admin'],
					media: [],
					published: []
				}
			);
		} else {
			console.log(res, 'user already exists!');
		}
	});
}

export function updateUser(data, func) {
	bcrypt.genSalt(10)
	.then((salt) => {
		bcrypt.hash(data.newPassword === '' ? data.password : data.newPassword, salt)
		.then((hashpass) => {
			data.newPassword = hashpass;
			rdb.table('users')
			.filter(rdb.row('name').eq(data.name))
			.update({
				password: data.newPassword,
				groups: JSON.parse(data.groups)
			})
			.run().then((res) => {
				func();
			});
		})
		.catch((err) => { console.log(err); });
	})
	.catch((err) => { console.log(err); });
}

export function createUserRDB(user, func) {
	bcrypt.genSalt(10).then((salt) => {
		bcrypt.hash(user.password, salt).then((hashpass) => {
			user.password = hashpass;
			rdb.table('users')
			.insert([ user ])
			.run().then((res) => {
				fs.access(PATHS.unpublished + user.name, fsC.F_OK).then(() => {
					func();
				})
				.catch((err) => {
					fs.mkdir(PATHS.unpublished + user.name).then(() => { 
						fs.access(PATHS.published + user.name, fsC.F_OK).then(() => { 
							func();
						})
						.catch((err) => {
							fs.mkdir(PATHS.published + user.name).then(() => { 
								func();
							})
							.catch((err) => { console.log(err); });
						});
					})
					.catch((err) => { console.log(err); });
				});
			});
		})
		.catch((err) => { console.log(err); });
	})
	.catch((err) => { console.log(err); });
}

export function updateUserMedia(type, user, func = () => {}) {
	const media = {};
	const folders = listFolder(
		type === 'media' ? 
		PATHS.unpublished + user : PATHS.published + user
	);

	folders.then((value) => {
		media[type] = value;
		rdb.table('users')
		.filter(rdb.row('name')
		.eq(user))
		.update(media)
		.run()
		.then((res) => {
				func();
				console.log(res);
		});
	});
}

export function clearTable(table) {
	rdb.table(table).delete().run().then((res) => {
		console.log(res);
	})
}
