'use strict';

import '../scss/main.scss';
import {Manager} from './modules/manager';

const main = document.querySelector('body');
const manager = new Manager();

main.classList.add('main');
main.appendChild(manager.container);
