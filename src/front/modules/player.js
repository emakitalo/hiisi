import * as Utils from './utilities';

class Player {
	constructor() {
		this.gui = document.createElement('DIV');
		this.audio = document.createElement('AUDIO');
		this.playlist = document.createElement('DIV');
		this.gui.appendChild(this.audio);
		this.gui.appendChild(this.playlist);
	}

	load(config) {
		config.context.appendChild(this.gui);
		this.playlist.getMedia(config.call + config.user + config.media).then((data) => {
			this.playlist.createList(
				config.playlist,
				config.path + config.user + config.media,
				data,
				config.context
			);
		});
	}
}

module.exports = {
	Player: Player
}
