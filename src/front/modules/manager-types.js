import Markdown from 'markdown-it';
import {html5Media} from 'markdown-it-html5-media';
import * as Utils from './utilities';
import * as PATHS from '../../back/paths';

const selectedElements = {};

export function Folder(folder_name = 'Undefined') {
	const container = Utils.createElement('UL', 'folder');
	const buttons = Utils.createElement('UL', 'folder-container');
	const name = Utils.createElement('LI', 'folder-name');
	const content = Utils.createElement('DIV', 'folder-content');

	container.setAttribute('name', folder_name);
	container.formdata = new FormData(); 

	name.innerHTML = folder_name;
	name.addEventListener('click', (e) => {
		Utils.setActiveFolder(e.target);
	});

	buttons.appendChild(name);
	container.appendChild(buttons);
	container.appendChild(content);

	return container;
}

export function File(name) {
	const label = Utils.createElement('DIV');
  label.innerHTML = name;
  const container = Utils.createElement('LI', 'file');
  container.setAttribute('name', name);
	container.addEventListener('click', (e) => {
		Utils.setActiveFile(container);		
	});
	container.appendChild(label)
	return container;
}

export class Buttons {
	constructor() {
		this.buttons = Utils.createElement('UL', 'button-panel');
	}
}

export class EditorButtons extends Buttons{
	constructor(editor) {
		super();
		this.buttons.classList.add('preview-panel');
		this.editor = editor;
		this.addMedia = Utils.createButton('add media');
		this.addMediaDialog = new AddMediaDialog();
		this.selectedFiles = [];
		this.save = Utils.createButton('save');
		this.close = Utils.createButton('close');
		this.activeFolder = Utils.selectActiveFolder;

		this.addMedia.addEventListener('click', (e) => {
			const media = Array.from(document.querySelectorAll(
				'li[name*=".mp3"], li[name*=".mp4"], li[name*=".png"]'
			));
			media.forEach((file) => {
				const name = file.getAttribute('name');
				const path = Utils.getPath(file.parentNode.parentNode) + name;
				const fileButton = Utils.createButton(path);	

				fileButton.addEventListener('click', (e) => {
					if(!this.selectedFiles.includes(path)) {
						fileButton.classList.add('media-selected');
						this.selectedFiles.push(path);
					} else {
						const id = this.selectedFiles.indexOf(path);
						fileButton.classList.remove('media-selected');
						this.selectedFiles.splice(id, 1);
					}
				});

				this.addMediaDialog.content.appendChild(fileButton);
			});
			this.addMediaDialog.container.classList.remove('hide');			
		});

		this.addMediaDialog.ok.addEventListener('click', (e) => {
			const files = [];
			this.selectedFiles.map((file) => {
				files.push('![' + file.split('/').pop() + '](/static/private/' + file + ') ');
			});
			this.editor.insert(files.join('\n'));
			this.addMediaDialog.container.classList.add('hide');
			this.addMediaDialog.content.innerHTML = '';
			this.selectedFiles = [];
		});

		this.addMediaDialog.cancel.addEventListener('click', (e) => {
			this.addMediaDialog.container.classList.add('hide');
			this.addMediaDialog.content.innerHTML = '';
			this.selectedFiles = [];
		});

		this.save.addEventListener('click', (e) => {
			if(!this.save.classList.contains('locked')) {
				const formdata = new FormData();
				const activeFile = document.querySelector('.active-file');
				const activeName = activeFile.getAttribute('name');
				const element = this.activeFolder.element;
				const content = this.activeFolder.content;
				const path = this.activeFolder.path;

				formdata.append('file', path + activeName);
				formdata.append('data', this.editor.getValue());

				const options = {
					method: 'POST',
					credentials: 'same-origin',
					body: formdata
				};

				this.save.classList.add('locked');

				fetch('api/save-file', options)
				.then(res => res.json())
				.then((res) => {
					this.save.classList.remove('locked');
					console.log(res.message);
				});
			}
		});

		this.close.addEventListener('click', (e) => {
			if(!this.save.classList.contains('locked')) {
				this.buttons.parentNode.classList.add('hide');
			}

		});

		this.addMediaDialog.container.classList.add('hide');

		this.buttons.appendChild(this.addMedia);
		this.buttons.appendChild(this.save);
		this.buttons.appendChild(this.close);
	}
}

export class FileButtons extends Buttons{
	constructor(clipboard, editor) {
		super();
		this.buttons.classList.add('file-buttons');
		this.buttons.classList.add('hide');
		this.preview = Utils.createButton('preview');
		this.edit = Utils.createButton('edit');
		this.editor = editor;
		this.rename = Utils.createButton('rename');
		this.cut = Utils.createButton('cut');
		this.remove = Utils.createButton('remove');
		this.publish = Utils.createButton('publish');
		this.previewDialog = new FilePreviewDialog();
		this.renameDialog = new TextInputDialog();
		this.removeDialog = new AcceptRejectDialog();
		this.activeFolder = Utils.selectActiveFolder;
		this.activeFile = () => { return document.querySelector('.active-file') };
		this.clipboard = clipboard;

		this.preview.addEventListener('click', (e) => {
			const activeFile = this.activeFile();
			const path = this.activeFolder.path;

			Utils.handleMedia(activeFile, path, this.previewDialog);
		});

		this.rename.addEventListener('click', (e) => {
			const activeFile = this.activeFile();
			this.renameDialog.input.value = activeFile.getAttribute('name');
			this.renameDialog.message.innerHTML = 'insert file name';	
			this.renameDialog.container.classList.remove('hide');
		});

		this.renameDialog.ok.addEventListener('click', (e) => {
			const formdata = new FormData();
			const activeFile = this.activeFile();
			const element = this.activeFolder.element;
			const content = this.activeFolder.content;
			const files = {};
			const oldName = this.activeFolder.path + activeFile.getAttribute('name');
			const newName = this.activeFolder.path + this.renameDialog.input.value;

			files[newName] = []; 
			files[newName].push(oldName);

			formdata.append('files', JSON.stringify(files));

			const options = {
				method: 'POST',
				credentials: 'same-origin',
				body: formdata
			};

			fetch('api/rename', options)
			.then(res => res.json())
			.then((res) => {
				const data = res.files;
				const keys = Object.keys(data);
				if(keys.length > 0) {
					const newName = this.renameDialog.input.value;
					activeFile.setAttribute('name', newName);
					activeFile.querySelector('div').innerHTML = newName; 
					activeFile.appendChild(this.buttons);
					this.renameDialog.container.classList.add('hide');
				} else {
					alert(res.message);
				}
			});
		});

		this.cut.addEventListener('click', (e) => {
			const path = this.activeFolder.path;
			const activeFile = this.activeFile();
			const activeName = activeFile.getAttribute('name');
			const keys = Object.keys(selectedElements);
			const clipButton = Utils.createButton(path + activeName);

			clipButton.setAttribute('name', path + activeName);
			clipButton.addEventListener('click', (e) => {
				let self = e.target;
				console.log(self.getAttribute('name'));
				delete selectedElements[self.getAttribute('name')];
				self.parentNode.removeChild(self);
			});

			if(keys.length > 0) {
				Object.keys(selectedElements).map((name) => {
					if(path + activeName !== name && !path.includes(name)){
						clipboard.container.appendChild(clipButton);
						selectedElements[path + activeName] = activeFile;
					}
				});
			} else {
				clipboard.container.appendChild(clipButton);
				selectedElements[path + activeName] = activeFile;
			}
		});

		this.remove.addEventListener('click', (e) => {
			const activeFile = this.activeFile();
			const activeName = activeFile.getAttribute('name');
			const path = this.activeFolder.path;

			this.removeDialog.container.classList.remove('hide');
			this.removeDialog.message.innerHTML = 'remove file ' + path + activeName;
		});

		this.removeDialog.ok.addEventListener('click', (e) => {
			const formdata = new FormData();
			const activeFile = this.activeFile();
			const activeName = activeFile.getAttribute('name');
			const element = this.activeFolder.element;
			const content = this.activeFolder.content;
			const path = this.activeFolder.path;

			formdata.append('file', path + activeName);
			const options = {
				method: 'POST',
				credentials: 'same-origin',
				body: formdata
			};

			fetch('api/remove-file', options)
			.then(res => res.json())
			.then((res) => {
				console.log(res);
				for(let i=0; i < content.children.length; i++) {
					const file = content.children[i];
					if(file.getAttribute('name') === activeName)
						content.removeChild(file);
				}
				this.buttons.classList.add('hide');
				document.querySelector('body').appendChild(this.buttons);
				this.removeDialog.container.classList.add('hide');
			});
		});

		this.edit.addEventListener('click', (e) => {
			const activeFile = this.activeFile();
			const activeName = activeFile.getAttribute('name');
			const ext = activeName.split('.').pop();
			if(Utils.mediaTypes[ext]) {
				const formdata = new FormData();
				formdata.append('file', this.activeFolder.path + activeName);

				const options = {
					method: 'POST',
					credentials: 'same-origin',
					body: formdata,
				};

				fetch('api/get-file', options)
					.then(res => {
						return res.text();
					})
					.then(data => {
						this.editor.setValue(data);
						this.editor.container.parentNode.classList.remove('hide');
					});
			}
		});

		this.publish.addEventListener('click', (e) => {
			const formdata = new FormData();

			formdata.append('file',
				this.activeFolder.path + this.activeFile().getAttribute('name') + '/'
			);

			const options = {
				method: 'POST',
				credentials: 'same-origin',
				body: formdata
			};

			fetch('api/publish', options)
			.then(res => res.json())
			.then((res) => {
				if(this.activeFile().classList.contains('published')) {
					this.activeFile().classList.remove('published');
					this.activeFile().querySelector('.file-buttons').lastChild.innerHTML = 'publish';
				} else {
					this.activeFile().classList.add('published');
					this.activeFile().querySelector('.file-buttons').lastChild.innerHTML = 'unpublish';
				}
			});
		});

		this.buttons.appendChild(this.preview);
		this.buttons.appendChild(this.edit);
		this.buttons.appendChild(this.rename);
		this.buttons.appendChild(this.cut);
		this.buttons.appendChild(this.remove);
		this.buttons.appendChild(this.publish);
	}
}

export class FolderButtons extends Buttons {
	constructor(clipboard) {
		super();
		this.buttons.classList.add('folder-buttons');
		this.input = Utils.createElement('INPUT');
		this.inputFolder = Utils.createElement('INPUT');
		this.add = Utils.createButton('new');
		this.rename = Utils.createButton('rename');
		this.cut = Utils.createButton('cut');
		this.paste = Utils.createButton('paste');
		this.remove = Utils.createButton('remove');
		this.addFiles = Utils.createElement('LABEL');
		this.addFolder = Utils.createElement('LABEL');
		this.publish = Utils.createButton('publish');
		this.addDialog = new TextInputDialog();
		this.fileOrFolder = Utils.createButton('insert folder name');
		this.renameDialog = new TextInputDialog();
		this.pasteDialog = new AcceptRejectDialog();
		this.removeDialog = new AcceptRejectDialog();
		this.activeFolder = Utils.selectActiveFolder;
		this.progressDialog = new ProgressDialog();
		this.clipboard = clipboard;

		this.fileOrFolder.setAttribute('folder', true);
		this.fileOrFolder.addEventListener('click', (e) => {
			const self = e.target;
			if(self.getAttribute('folder') !== 'false') {
				self.innerHTML = 'insert file name';
				self.setAttribute('folder', false)
			} else {
				self.innerHTML = 'insert folder name';
				self.setAttribute('folder', true);
			}
		});
		this.addDialog.message.appendChild(this.fileOrFolder);

		this.add.addEventListener('click', (e) => {
			this.addDialog.container.classList.remove('hide');
			document.querySelector('.folder-buttons').classList.add('hide');
		});

		this.addDialog.ok.addEventListener('click', (e) => {
			const formdata = new FormData();
			const element = this.activeFolder.element;
			const content = this.activeFolder.content;
			const path = this.activeFolder.path;

			formdata.append(
				this.fileOrFolder.getAttribute('folder') === 'true' ? 'folder' : 'file',
				path + this.addDialog.input.value
			);

			const options = {
				method: 'POST',
				credentials: 'same-origin',
				body: formdata
			};

			fetch('api/add-folder', options)
			.then(res => res.json())
			.then((res) => {
				if(res.state) {
					Utils.appendElement('.folder', Folder(this.addDialog.input.value), content);
				} else {
					Utils.appendElement('.folder', File(this.addDialog.input.value), content);
				}
				this.addDialog.container.classList.add('hide');
			});
		});

		this.rename.addEventListener('click', (e) => {
			const element = this.activeFolder.element;
			const content = this.activeFolder.content.parentNode.parentNode;
			this.renameDialog.message.innerHTML = 'insert folder name';	
			this.renameDialog.container.classList.remove('hide');
			document.querySelector('.folder-buttons').classList.add('hide');
		});

		this.renameDialog.ok.addEventListener('click', (e) => {
			const formdata = new FormData();
			const element = this.activeFolder.element;
			const content = this.activeFolder.content.parentNode.parentNode;
			const files = {};
			const oldName = this.activeFolder.path;
			const newName = oldName.split('/');
			newName.splice(newName.length - 2, 2);
			newName.push(this.renameDialog.input.value);
			newName.push('');

			files[newName.join('/')] = []; 
			files[newName.join('/')].push(oldName);

			formdata.append('files', JSON.stringify(files));

			const options = {
				method: 'POST',
				credentials: 'same-origin',
				body: formdata
			};

			fetch('api/rename', options)
			.then(res => res.json())
			.then((res) => {
				const data = res.files;
				const keys = Object.keys(data);
				if(keys.length > 0) {
					const newPath = keys[0].split('/');
					const newName = newPath[newPath.length - 2];
					element.setAttribute('name', newName);
					element.querySelector('.folder-name').innerHTML = newName; 
					Utils.appendElement('.folder', element, content);
					this.renameDialog.container.classList.add('hide');
				} else {
					alert(res.message);
				}
			});
		});

		this.remove.addEventListener('click', (e) => {
			const root = this.activeFolder.element.parentNode.parentNode;
			if(root.classList.contains('folder')) {
				const path = this.activeFolder.path;
				this.removeDialog.container.classList.remove('hide');
				this.removeDialog.message.innerHTML = 'remove folder ' + path;
			} else {
				alert('root folder cannot be removed');
			}
			document.querySelector('.folder-buttons').classList.add('hide');
		});

		this.cut.addEventListener('click', (e) => {
			const path = this.activeFolder.path;
			const keys = Object.keys(selectedElements);
			const clipButton = Utils.createButton(path);
			clipButton.setAttribute('name', path);
			clipButton.addEventListener('click', (e) => {
				let self = e.target;
				delete selectedElements[self.getAttribute('name')];
				self.parentNode.removeChild(self);
			});

			if(keys.length > 0) {
				keys.map((name) => {
					if(path !== name && !name.includes(path)) {
						clipboard.container.appendChild(clipButton);
						selectedElements[path] = this.activeFolder.element;
					}
				});
			} else {	
				clipboard.container.appendChild(clipButton);
				selectedElements[path] = this.activeFolder.element;
			}
			document.querySelector('.folder-buttons').classList.add('hide');
		});

		this.paste.addEventListener('click', (e) => {
			this.pasteDialog.message.innerHTML = 'paste selected files';	
			this.pasteDialog.message.addEventListener('click', (e) => {
				const self = e.target;
				if(self.innerHTML.includes('paste')) {
					self.innerHTML = 'duplicate selected files';
				} else {
					self.innerHTML = 'paste selected files';
				}
			});
			this.pasteDialog.container.classList.remove('hide');
			document.querySelector('.folder-buttons').classList.add('hide');
		});

		this.pasteDialog.ok.addEventListener('click', (e) => {
			const path = this.activeFolder.path;
			const formdata = new FormData();
			const files = {};
			let filesCount = 0;
			
			Object.keys(selectedElements).some((key) => {
				const oldPath = key.split('/');
				const name = selectedElements[key].classList.contains('file') ? 
					oldPath[oldPath.length - 1] : oldPath.splice(oldPath.length - 2, 1
				);
				files[path + name] = [];
				if(!(path).match(new RegExp('^' + key)) || path > key.length) {
					files[path + name].push(key);
					filesCount++;
				} else {
					delete selectedElements[key];
				}
			});

			if(filesCount > 0) {
				if(this.pasteDialog.message.innerHTML.includes('duplicate')) {
					formdata.append('duplicate', 'true');
				}

				formdata.append('files', JSON.stringify(files));

				const options = {
					method: 'POST',
					credentials: 'same-origin',
					body: formdata
				};

				fetch('api/rename', options)
				.then(res => res.json())
				.then((res) => {
					let locked = false;
					Object.keys(selectedElements).some((key) => {
						if(!this.activeFolder.path.match(new RegExp('^' + key)) || 
						this.activeFolder.path.length < key.length) {
							const folder = selectedElements[key];
							if(res.duplicate) {
								location.reload();
							} else {
								if(folder.classList.contains('published') && !locked) {
									const length = this.activeFolder.path.split('/').length - 2;
									let parent = this.activeFolder.element;
									this.activeFolder.buttons.lastChild.innerHTML = 'unpublish';
									for(let i=0; i < length; i++) {
										parent.classList.add('published');
										parent = parent.parentNode.parentNode;
									}
									locked = true;
								}
								folder.classList.remove('hide');
								this.activeFolder.content.appendChild(folder);
							}
							delete selectedElements[key];
							this.clipboard.container.removeChild(
								this.clipboard.container.querySelectorAll('li[name="' + key + '"]')[0]
							)
						}
						this.pasteDialog.container.classList.add('hide');
					});
				});
			} else {
				this.pasteDialog.container.classList.add('hide');
			}
		});

		this.removeDialog.ok.addEventListener('click', (e) => {
			const formdata = new FormData();
			const element = this.activeFolder.element;
			const content = this.activeFolder.content;
			const path = this.activeFolder.path;

			formdata.append('folder', path);

			const options = {
				method: 'POST',
				credentials: 'same-origin',
				body: formdata
			};

			fetch('api/remove-folder', options)
			.then(res => res.json())
			.then((res) => {
				const parent = element.parentNode;
				const navigation = parent.parentNode.querySelector('.folder-container');
				const fileButtons = element.querySelector('.file-buttons');
				const folderButtons = element.querySelector('.folder-buttons');
				parent.parentNode.classList.add('active-folder');
				navigation.appendChild(folderButtons);
				fileButtons ? document.querySelector('body').appendChild(fileButtons) : null;
				parent.removeChild(element);
				const subNodes = parent.children;
				for(let i=0; i < subNodes.length; i++) {
					const node = subNodes[i];
					node.classList.remove('hide');
				}
				this.removeDialog.container.classList.add('hide');
			});
		});

		this.addFiles.innerHTML = 'upload files';
		this.addFiles.htmlFor = 'add-file';
		this.addFiles.classList.add('button');
		this.addFiles.addEventListener('click', (e) => {
			this.input.webkitdirectory = false;
			this.input.mozdirectory = false;
			document.querySelector('.folder-buttons').classList.add('hide');
		});

		this.addFolder.innerHTML = 'upload folder';
		this.addFolder.htmlFor = 'add-file';
		this.addFolder.classList.add('button');
		this.addFolder.addEventListener('click', (e) => {
			this.input.webkitdirectory = true;
			this.input.mozdirectory = true;
			document.querySelector('.folder-buttons').classList.add('hide');
		});

		this.input.id = 'add-file';
		this.input.type = 'file';
		this.input.multiple = true;
		this.input.addEventListener('change', (e) => {
			console.log('FILUT: ', this.input.files);
			const element = this.activeFolder.element;
			const content = this.activeFolder.content;
			const path = this.activeFolder.path;

			for(let id in this.input.files) {
				const data = this.input.files[id];
				const filePath = path + '/' + (this.input.webkitdirectory ? data.webkitRelativePath : data.name);
				if(!element.formdata.has(filePath) && typeof data === 'object') {
					element.formdata.append(
						filePath,
						data,
						data.name
					);
				} else {
					// Toggle
				}
			}

			const options = {
				method: 'POST',
				credentials: 'same-origin',
				body: element.formdata
			};

			const request = new XMLHttpRequest();

			request.onreadystatechange = () => {
				if(request.readyState == XMLHttpRequest.DONE) {
					const res = JSON.parse(request.responseText);
					console.log('RESPONSE', res);
					console.log('RES', res.files);
					if(this.input.webkitdirectory) {
						Utils.renderDirTree(
							res.files,
							this.activeFolder.element,
							this.activeFolder.content,
							path.split('/').length - 2
						);
					} else {
						for (let id in this.input.files) {
							const file = this.input.files[id];
							if (typeof file === 'object') {
								if(!Utils.checkFileExists(file.name, content.children)) {
									const label = File(file.name);
									content.appendChild(label);
								}
							}
						}
					}
					this.progressDialog.container.classList.add('hide');
					this.input.value = null;
				}
			}

			request.upload.addEventListener('loadstart', (e) => {
				this.progressDialog.dialog.innerHTML = (0).toString() + '%';
				this.progressDialog.container.classList.remove('hide');
			});

			request.upload.addEventListener('progress', (e) => {
				if(e.lengthComputable) {
					console.log('PROGRESS:', );
					this.progressDialog.dialog.innerHTML = Math.round(e.loaded / e.total * 100).toString() + '%';
				} else {
					
				}
			});

			request.upload.addEventListener('load', (res) => {
			});

			request.open('POST', 'api/upload');
			request.send(element.formdata);
		});


		this.publish.addEventListener('click', (e) => {
			const formdata = new FormData();

			formdata.append('file', this.activeFolder.path);

			const options = {
				method: 'POST',
				credentials: 'same-origin',
				body: formdata
			};

			fetch('api/publish', options)
			.then(res => res.json())
			.then((res) => {
				const publishBtn = this.activeFolder.buttons.lastChild;
				if(this.activeFolder.element.classList.contains('published')) {
					publishBtn.innerHTML = 'publish';
					const pub = Array.from(this.activeFolder.element.querySelectorAll('.published'));
					pub.forEach((item) => {
						item.classList.remove('published');
					});
					this.activeFolder.element.classList.remove('published');

				} else {
					publishBtn.innerHTML = 'unpublish';
					const path = Utils.getPath(this.activeFolder.element).split('/').slice(1,-1);
					let active = this.activeFolder.element;
					for(let i=path.length; i > 0; i--) {
						active.classList.add('published');
						active = active.parentNode.parentNode;
					}
				}
			});
			document.querySelector('.folder-buttons').classList.add('hide');
		});

		this.buttons.appendChild(this.add);
		this.buttons.appendChild(this.rename);
		this.buttons.appendChild(this.cut);
		this.buttons.appendChild(this.paste);
		this.buttons.appendChild(this.remove);
		this.buttons.appendChild(this.addFiles);
		this.buttons.appendChild(this.addFolder);
		this.buttons.appendChild(this.publish);
	}
}

export class Container {
	constructor() {
		this.container = Utils.createElement('DIV', 'fullscreen-container');
		this.container.classList.add('hide');
	}
}

export class ContainerCenter extends Container {
	constructor() {
		super();
		this.center = Utils.createElement('DIV', 'center');
		this.container.appendChild(this.center);
	}
}

export class ProgressDialog extends ContainerCenter {
	constructor() {
		super();
		this.center.classList.add('middle');
		this.dialog = Utils.createElement('DIV', 'dialog-info');
		this.center.appendChild(this.dialog);
	}
}

export class AcceptRejectDialog extends ContainerCenter {
	constructor() {
		super();
		this.center.classList.add('middle');
		this.dialog = Utils.createElement('DIV', 'dialog-info');
		this.message = document.createElement('P');
		this.ok = Utils.createButton('ok');
		this.cancel = Utils.createButton('cancel');

		this.cancel.addEventListener('click', (e) => {
			this.container.classList.add('hide');
		});

		this.dialog.appendChild(this.message);
		this.dialog.appendChild(this.ok);
		this.dialog.appendChild(this.cancel);
		this.center.appendChild(this.dialog);
	}
}

export class TextInputDialog extends ContainerCenter{
	constructor() {
		super();
		this.center.classList.add('middle');
		this.dialog = Utils.createElement('DIV', 'dialog-info');
		this.message = document.createElement('P');
		this.input = document.createElement('INPUT');
		this.ok = Utils.createButton('ok');
		this.cancel = Utils.createButton('cancel');

		this.input.setAttribute('type', 'text');

		this.cancel.addEventListener('click', (e) => {
			this.container.classList.add('hide');
			this.input.value = '';
		});

		this.dialog.appendChild(this.message);
		this.dialog.appendChild(this.input);
		this.dialog.appendChild(this.ok);
		this.dialog.appendChild(this.cancel);
		this.center.appendChild(this.dialog);
	}	
}

export class FilePreviewDialog extends ContainerCenter{
	constructor() {
		super();
		this.panel = Utils.createElement('DIV', 'preview-panel');
		this.name = Utils.createElement('DIV', 'preview-label');
		this.buttonPanel = Utils.createElement('UL', 'button-panel');
		this.close = Utils.createButton('CLOSE');

		this.audio = new Audio();
		this.image = new Image();
		this.video = document.createElement('VIDEO');
		this.md = new Markdown({
  		html: true,
  		linkify: true,
  		typographer: true
		});
		this.mdPreview = Utils.createElement('DIV', 'md-preview');

		this.md.use(html5Media);

		this.audio.controls = true;
		this.video.controls = true;

		this.audio.classList.add('hide');
		this.image.classList.add('hide');
		this.video.classList.add('hide');
		this.mdPreview.classList.add('hide');

		this.buttonPanel.appendChild(this.close);
		this.panel.appendChild(this.name);
		this.panel.appendChild(this.buttonPanel);

		this.center.appendChild(this.panel);
		this.center.appendChild(this.audio);
		this.center.appendChild(this.image);
		this.center.appendChild(this.video);
		this.center.appendChild(this.mdPreview);
	}
}

export class AddMediaDialog {
	constructor() {
		this.container = Utils.createElement('DIV', 'add-media-dialog');
		this.ok = Utils.createButton('ok');
		this.cancel = Utils.createButton('cancel');
		this.buttonPanel = Utils.createElement('UL', 'button-panel');
		this.content = Utils.createElement('DIV', 'edit-content');

		this.container.classList.add('hide');
		
		this.buttonPanel.appendChild(this.ok);
		this.buttonPanel.appendChild(this.cancel);

		this.container.appendChild(this.buttonPanel);
		this.container.appendChild(this.content);
	}
}

export class UserSettings {
	constructor() {
		this.container = Utils.createElement('DIV', 'user-properties');
		this.userSelect = Utils.createElement('SELECT', 'select-users');
		this.users = {};
		this.nameLabel = Utils.createElement('LABEL');
		this.nameLabel.innerHTML = 'username';
		this.nameLabel.for = 'user-name';
		this.name = Utils.createElement('INPUT');
		this.name.id = 'user-name';
		this.passwordLabel = Utils.createElement('LABEL');
		this.passwordLabel.innerHTML = 'password';
		this.passwordLabel.for = 'password';
		this.password = Utils.createElement('INPUT');
		this.password.id = 'password';
		this.newPasswordLabel = Utils.createElement('LABEL');
		this.newPasswordLabel.innerHTML = 'new password';
		this.newPasswordLabel.for = 'new-password';
		this.newPasswordLabel.classList.add('hide');
		this.newPassword = Utils.createElement('INPUT');
		this.newPassword.id = 'new-password';
		this.newPassword.classList.add('hide');
		this.groups = ['admin', 'wheel'];
		this.groupsContainer = Utils.createElement('DIV');
		this.groupsContainer.classList.add('user-groups');
		this.updateUser = Utils.createElement('DIV');
		this.updateUser.classList.add('button');
		this.updateUser.classList.add('active');
		this.updateUser.innerHTML = 'update';
		this.createUser = true;

		this.groups.map(group => {
			const container = Utils.createElement('DIV');
			const button = Utils.createElement('INPUT');
			const label = Utils.createElement('LABEL');
			button.id = group;
			button.type = 'checkbox';
			button.checked = false;
			label.for = group;
			label.innerHTML = group;
			container.appendChild(button);
			container.appendChild(label);
			this.groupsContainer.appendChild(container);
		});

		this.updateUser.addEventListener('click', (e) => {
			const formdata = new FormData();			
			const groups = [];

			Array.from(this.groupsContainer.querySelectorAll('input')).forEach(group => {
				if(group.checked)
					groups.push(group.id);
			})

			const options = {
				method: 'POST',
				credentials: 'same-origin',
				body: formdata
			};

			formdata.append('groups', JSON.stringify(groups));

			if(this.createUser) {
				if(Object.keys(this.users).includes(this.name.value)) {
					alert('User ' + this.name.value + ' already exists!');
				} else {
					formdata.append('name', this.name.value);
					formdata.append('password', this.password.value);
					fetch('api/create-user', options)
					.then(res => res.json())
					.then(data => {
						const name = Object.keys(data)[0]
						this.users[name] = data[name];
						const userOption = Utils.createElement('OPTION', 'option-user');
						userOption.text = name;
						this.userSelect.appendChild(userOption);
					})
				}
			} else {
				formdata.append('name', this.name.value);
				formdata.append('password', this.password.value);
				formdata.append('newPassword', this.newPassword.value);

				options.method = 'PUT';
				fetch('api/update-user', options)
				.then(res => { location.href =  'login'; } )
			}
		})

		this.container.appendChild(this.userSelect);
		this.container.appendChild(this.nameLabel);
		this.container.appendChild(this.name);
		this.container.appendChild(this.passwordLabel);
		this.container.appendChild(this.password);
		this.container.appendChild(this.newPasswordLabel);
		this.container.appendChild(this.newPassword);
		this.container.appendChild(this.groupsContainer);
		this.container.appendChild(this.updateUser);
	}
}

export class Clipboard {
	constructor() {
		this.container = Utils.createElement('UL', 'clipboard');
	}
}
