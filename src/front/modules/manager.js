import * as Utils from './utilities';
import * as Types from './manager-types';
import * as Templates from './templates';
import 'ace-builds';
import 'ace-builds/src-noconflict/mode-javascript';
import 'ace-builds/src-noconflict/mode-markdown';
import 'ace-builds/src-noconflict/theme-dracula';
import 'ace-builds/webpack-resolver';
import * as PATHS from '../../back/paths';

export class Manager {
	constructor() {
		this.container = Utils.createElement('DIV', 'container');
		this.content = Utils.createElement('DIV', 'content');
		this.navigation = Utils.createElement('UL', 'main-navigation');

		this.clipboard = new Types.Clipboard();

		this.aceContainer = Utils.createElement('DIV', 'ace-editor-container');
		this.aceContainer.classList.add('hide');
		this.editorContainer = Utils.createElement('DIV', 'ace-editor');
		this.editor = ace.edit(this.editorContainer, {
			mode: 'ace/mode/markdown',
			theme: 'ace/theme/dracula',
			fontSize: 14
		});

		this.editorGui = new Types.EditorButtons(this.editor);
		this.aceContainer.appendChild(this.editorGui.buttons);
		this.aceContainer.appendChild(this.editorContainer);
		this.aceContainer.appendChild(this.editorGui.addMediaDialog.container);

		this.views = {
			media: new Templates.Media(this),
			users: new Templates.Users(this),
		};

		this.buttons = {
			title: Utils.createButton('LVL001'),
			media: Utils.createButton('media'),
			preview: Utils.createButton('preview'),
			users: Utils.createButton('users'),
			logout: Utils.createButton('logout')
		};

		this.buttons.media.addEventListener('click', (e) => {
			Utils.setActiveView('media', e.target, this.views.media.form, this.content);
			this.views.media.init();
		});

		this.buttons.preview.addEventListener('click', (e) => {
			location.href = PATHS.subaddr + '/' + user;
		});

		this.buttons.users.addEventListener('click', (e) => {
			Utils.setActiveView('users', e.target, this.views.users.form, this.content);
			this.views.users.init();
		});

		this.buttons.logout.addEventListener('click', (e) => {
			location.href = 'logout';
		});

		Object.keys(this.buttons).map((k, i) => {
			this.navigation.appendChild(this.buttons[k]);
		})

		this.container.appendChild(this.navigation);
		this.container.appendChild(this.content);

		let activeView = localStorage.getItem('active-view');

		this.views[activeView] ? activeView = activeView : activeView = 'media';
		this.views[activeView].init();

		Utils.setActiveView(
			activeView ? activeView : 'media',
			activeView ? this.buttons[activeView] : this.buttons.media,
			activeView ? 
				this.views[activeView].form ? 
					this.views[activeView].form : this.views[activeView].container : 
				this.views.media.form ? this.views.media.form : this.views.media.container,
			this.content
		);
	}
} 
