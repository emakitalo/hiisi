class Audio {
	constructor(url) {
		this.source = document.createElement('SOURCE');
		this.source.src = url;
	}
}

module.exports = {
	Audio: Audio
}
