import * as Types from './manager-types';
import * as PATHS from '../../back/paths';

const video = 'video';
const image = 'image';
const audio = 'audio';
const text = 'text';

export const mediaTypes = {
  mp4: video,
  avi: video,
  mkv: video,
  mp3: audio,
  png: image,
  jpg: image,
  svg: image,
  md: text,
  js: text,
  html: text,
  json: text,
	css: text,
	hbs: text,
};

const coreButtons = [
	'rename',
	'cut',
	'remove',
	'publish',
	'unpublish'
]

const allButtons = [
	'preview',
	'edit',
].concat(coreButtons);

const reducedButtons = [
	'preview',
].concat(coreButtons);

const rootButtons = [
	'new',
	'paste',
	'add files'
];

export const fileButtons = {
	text: allButtons,
	video: reducedButtons,
	image: reducedButtons,
	audio: reducedButtons,
	default: coreButtons
};

export function addListeners(element, eventListeners) {
  for (let event in eventListeners) {
    const listeners = eventListeners[event];
    listeners.map(listener => element.addEventListener(event, listener));
  }
}

export function createElement(type, style) {
  const element = document.createElement(type);
  style ? element.classList.add(style) : '';
  return element;
}

export function createButton(name) {
  const button = createElement('LI', 'button');
  button.innerHTML = name;
  return button;
}

export function setButtonActive(target) {
  const parent = Array.from(target.parentNode.querySelectorAll('li'));
  parent.forEach(child => child.classList.remove('active'));
  target.classList.add('active');
}

export function getMedia(url) {
  return fetch(url, {
			credentials: 'same-origin'
		}).then(re => {
    return re.json();
  });
}

export function beautifyName(name) {
  return name
    .split('.')[0]
    .split('_')
    .join(' ');
}

export function setActiveView(name, button, view, context) {
  localStorage.setItem('active-view', name);
  setButtonActive(button);
  while (context.firstChild) context.removeChild(context.firstChild);
  context.appendChild(view);
  //history.
}

export function infoDialog(message) {
  const container = document.createElement('DIV');
  const dialog = document.createElement('DIV');
  const close = document.createElement('DIV');

  container.classList.add('dialog-info');
  dialog.innerHTML = message;

  close.innerHTML = 'CLOSE';
  close.classList.add('button-close');
  close.addEventListener('click', e => {
    const self = e.target.parentNode;
    self.parentNode.removeChild(self);
  });

  container.appendChild(close);
  container.appendChild(dialog);
  return container;
}

export function acceptDialog(message, func) {
  const container = document.createElement('DIV');
  const dialog = document.createElement('DIV');
  const ok = document.createElement('DIV');
  const cancel = document.createElement('DIV');

  container.classList.add('dialog-info');
  dialog.innerHTML = message;

  ok.innerHTML = 'OK';
  ok.classList.add('button');
  ok.addEventListener('click', e => {
    const self = e.target.parentNode;
    self.parentNode.removeChild(self);
    func();
  });

  cancel.innerHTML = 'CANCEL';
  cancel.classList.add('button');
  cancel.addEventListener('click', e => {
    const self = e.target.parentNode;
    self.parentNode.removeChild(self);
  });

  container.appendChild(dialog);
  container.appendChild(ok);
  container.appendChild(cancel);
  return container;
}

export function setActiveFolder(button) {
	const activeFolder = selectActiveFolder;
	const folder = activeFolder.element;
	const path = activeFolder.path;
	const buttons = activeFolder.buttons;
	const self = button.parentNode;
	const selfPath = getPath(self.parentNode);
	const publishBtn = buttons.lastChild;

	localStorage.setItem('path', selfPath);

	if(path !== selfPath) {
		const activeFiles = activeFolder.content;
		const selfFiles = self.parentNode.querySelector('.folder-content');

		for(let i=0; i < activeFiles.children.length; i++) {
			const file = activeFiles.children[i];
			if(getPath(file) !== selfPath) {
				file.classList.add('hide');
			}
		}

		for(let i=0; i < selfFiles.children.length; i++) {
			const file = selfFiles.children[i];
			if(file.classList.contains('hide')) {
				file.classList.remove('hide');
			}
		}

		if(path.length > selfPath.length) {
			let activeFolders = path.split('/');
			let selfFolders = selfPath.split('/');
			const start = selfFolders.length - 1;
			const offset = activeFolders.length - selfFolders.length;
			for(let i=start; i < start + offset; i++) {
				const lastNodes = selfFiles.querySelector("ul[name='" + activeFolders[i] + "']").querySelector('.folder-content').children;
				for(let j=0; j < lastNodes.length; j++) {
					const node = lastNodes[j];
					node.classList.add('hide');
				}
			}

			if(offset === 1) {
				const lastNodes = selfFiles.querySelector('.folder-content').children;
				for(let i=0; i < lastNodes.length; i++) {
					const node = lastNodes[i];
					node.classList.add('hide');
				}
			} else {
				folder.classList.add('hide');
			}
		}
	}

	if(self.parentNode.classList.contains('active-folder')) {
		if(buttons.classList.contains('hide')) {
			document.querySelector('.file-buttons').classList.add('hide');
			buttons.classList.remove('hide');
		} else {
			buttons.classList.add('hide');
		}
	} else {
		if(!buttons.classList.contains('hide'))
			buttons.classList.add('hide');
	}

	folder.classList.remove('active-folder');
	self.parentNode.classList.add('active-folder');

	if(self.parentNode.classList.contains('published')) {
		publishBtn.innerHTML = 'unpublish';
	} else {
		publishBtn.innerHTML = 'publish';
	}

	Array.from(buttons.querySelectorAll('li')).forEach((btn) => {
		const currentFolder = button.parentNode.parentNode;
		const parentFolder = currentFolder.parentNode.parentNode;
		if(!parentFolder.classList.contains('folder')) {
			if(rootButtons.includes(btn.innerHTML))
				btn.classList.remove('hide');
			else
				btn.classList.add('hide');
		} else {
			btn.classList.remove('hide');
		}
	});

	self.appendChild(buttons);
}

export function setActiveFile(file) {
  const activeFile = document.querySelector('.active-file');
	const ext = file.getAttribute('name').split('.').pop();
  const buttons = document.querySelector('.file-buttons');
	const allowedButtons = fileButtons[mediaTypes[ext]] ? 
		fileButtons[mediaTypes[ext]] : fileButtons.default;

	if(file.classList.contains('published')) {
		buttons.lastChild.innerHTML = 'unpublish';
	} else {
		buttons.lastChild.innerHTML = 'publish';
	}

	Array.from(buttons.querySelectorAll('li')).forEach((button) => {
		if(allowedButtons.includes(button.innerHTML))
			button.classList.remove('hide');
		else
			button.classList.add('hide');
	});

	if(buttons.classList.contains('hide') || !file.classList.contains('active-file')) {
		document.querySelector('.folder-buttons').classList.add('hide');
		buttons.classList.remove('hide');
	} else {
		buttons.classList.add('hide');
	}

	if(activeFile)
		activeFile.classList.remove('active-file');

  file.classList.add('active-file');
  file.appendChild(buttons);
}

export function handleMedia(label, path, preview) {
	const previewPath = 'static/private/' + path;
  const name = label.getAttribute('name');
  const ext = name.split('.').pop();

  preview.container.classList.remove('hide');
  preview.name.innerHTML = name;

  switch (mediaTypes[ext]) {
    case image:
			preview.image.src = previewPath + name;
      preview.image.classList.remove('hide');
      preview.close.addEventListener('click', e => {
        preview.image.src = '';
        preview.image.classList.add('hide');
        preview.container.classList.add('hide');
      });
      break;
    case video:
      preview.video.src = previewPath + name;
      preview.video.classList.remove('hide');
      preview.close.addEventListener('click', e => {
        preview.video.src = '';
        preview.video.classList.add('hide');
        preview.container.classList.add('hide');
      });
      break;
    case audio:
      preview.audio.src = previewPath + name;
      preview.audio.classList.remove('hide');
      preview.close.addEventListener('click', e => {
        preview.audio.src = '';
        preview.audio.classList.add('hide');
        preview.container.classList.add('hide');
      });
      break;
    case text:
			if(ext === 'html') {
				window.location.assign(previewPath + name);
			} else {
				const formdata = new FormData();
				formdata.append('file', path + name);

				const options = {
					method: 'POST',
					body: formdata,
				};

				fetch((PATHS.subaddr.length > 2 ? PATHS.subaddr : '') + '/api/get-file', options)
					.then(res => {
						return res.text();
					})
					.then(data => {
						preview.mdPreview.innerHTML = preview.md.render(data);
						preview.mdPreview.classList.remove('hide');
						preview.close.addEventListener('click', e => {
							preview.mdPreview.innerHTML = '';
							preview.mdPreview.classList.add('hide');
							preview.container.classList.add('hide');
						});
					});
				break;
		}
	}
}

export function renderDirTree(data, folder, content, depth = 0) {
  let exFile;
  const path = getPath(folder);
  for (let i = 0; i < data.length; i++) {
    if (typeof data[i] !== 'object') {
      let fileExists = false;
			Array.from(content).forEach(file => {
				if(file.getAttribute('name') === data[i]) {
					fileExists = true;	
				}
			})
      if (!fileExists) {
        const file = Types.File(data[i]);
        if (path.split('/').length > 2) file.classList.add('hide');
        content.appendChild(file);
      }
    } else {
      const name = Object.keys(data[i])[0];
      const newData = data[i][name];
			let oldFolder = false;
			Array.from(content.children).forEach(item => {
				if(item.getAttribute('name') === name) {
					oldFolder = item;
				}
			})
      const newFolder = oldFolder ? oldFolder : new Types.Folder(name, path);
      const newContent = newFolder.querySelector('.folder-content');

      for (let i = 0; i < content.children.length; i++) {
        const node = content.children[i];
        if (node.classList.contains('file')) {
          exFile = node;
          break;
        }
      }

			if (path.split('/').length - depth > 2) {
				newFolder.classList.add('hide');
			}

      if (!oldFolder) {
        if (exFile) {
          content.insertBefore(newFolder, exFile);
        } else {
          content.appendChild(newFolder);
        }
      }

      renderDirTree(newData, newFolder, newContent, depth);
    }
  }
}

export function checkPublished(data, folder, content) {
  for (let i = 0; i < data.length; i++) {
    if (typeof data[i] !== 'object') {
      const fileExists = content.querySelectorAll(
        "li[name='" + data[i] + "']",
      )[0];
      if (fileExists) {
				fileExists.classList.add('published');
      }
    } else {
      const name = Object.keys(data[i])[0];
      const newData = data[i][name];
      const newFolder = content.querySelectorAll("ul[name='" + name + "']")[0];
      const newContent = newFolder.querySelector('.folder-content');
			
			newFolder.classList.add('published');

      checkPublished(newData, newFolder, newContent);
    }
  }
}

export function followPath(pathTo) {
  while (pathTo.length > 1) {
    const activeFolder = selectActiveFolder;
    const content = activeFolder.content;
    const name = pathTo.pop();
    const element = content
      .querySelector("ul[name='" + name + "']")
      .querySelector('.folder-name');
    setActiveFolder(element);
  }
}

export function checkFileExists(name, content) {
  for (let i = 0; i < content.length; i++) {
    const file = content[i];
    const fileName = file.getAttribute('name');
    if (name === fileName) {
      return true;
    }
  }
  return false;
}

export function getPath(element) {
  try {
    let parent = element;
    let path = ['', parent.querySelector('.folder-name').innerHTML];
    while (parent.parentNode.parentNode.classList.contains('folder')) {
      parent = parent.parentNode.parentNode;
      path.push(parent.querySelector('.folder-name').innerHTML);
    }
    return path.reverse().join('/');
  } catch (err) {
    return err;
  }
}

export const selectActiveFolder = {
  get element() {
    return document.querySelector('.active-folder');
  },
  get content() {
    return this.element.querySelector('.folder-content');
  },
  get path() {
		return getPath(this.element);
  },
  get buttons() {
    return this.element.querySelector('.button-panel');
  },
};

export function appendElement(type, newElement, content) {
  const elements = content.querySelectorAll(':scope > ' + type);
  const name = newElement.getAttribute('name');
	if(elements.length > 0) {
		for(let i=0; i < elements.length; i++) {
			const element = elements[i];
			const eName = element.getAttribute('name');
			if (eName.localeCompare(name) > 0) {
				content.insertBefore(newElement, element);
				break;
			} else if(i === elements.length -1) {
				content.appendChild(newElement);
			} 
		}
	} else {
		content.appendChild(newElement);
	}
}
