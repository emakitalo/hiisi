import {Audio} from './audio';

class Gui {
	constructor() {
		this.container = document.createElement('DIV');
		this.lists = {};
	}

	init() {
		const upload = [
			{'name'}
		];

		this.container.appendChild(
			createForm({'name':'type'})
		);
	}

	createList(name, path, data, list) {
		console.log(data);
		data.map((item) => {
			if(typeof item === 'object') {
				Object.keys(item).map((key, index) => {
					const avatar = document.createElement('IMG');
					const label = document.createElement('H3');
					const newList = document.createElement('UL');
					avatar.src = path + '/' + key + '/cover.png' ;
					label.innerHTML = key;
					list.appendChild(avatar);
					list.appendChild(label);
					list.appendChild(newList);
					this.createList(name, path + '/' + key, item[key], newList);
				});
			} else {
				const file = document.createElement('LI');
				file.innerHTML = this.beautifyName(item);
				file.setAttribute('track', path + '/' + item);
				file.addEventListener('click', (e) => {
					const self = e.target;
					this.player.src = self.getAttribute('track');
					this.player.load();
					this.player.play();
				});
				list.appendChild(file);
			}
		});
		this.lists[name] = list;
	}

}

module.exports = {
	Gui: Gui,
}
