import * as Utils from './utilities';
import * as Types from './manager-types';
import * as PATHS from '../../back/paths';
import {init as mediaplayer } from './mediaplayer';

export class Media {
	constructor(parent) {
		this.root = document.querySelector('BODY');
		this.form = Utils.createElement('FORM');

		this.parent = parent;

		this.rootFolder = new Types.Folder(user);
		this.rootContent = this.rootFolder.querySelector('.folder-content');
		this.rootFolder.classList.add('active-folder');

		this.activeFolder = Utils.selectActiveFolder;

		this.folderGui = new Types.FolderButtons(this.parent.clipboard);
		this.fileGui = new Types.FileButtons(this.parent.clipboard, this.parent.editor);

		this.root.appendChild(this.folderGui.addDialog.container);
		this.root.appendChild(this.folderGui.renameDialog.container);
		this.root.appendChild(this.folderGui.pasteDialog.container);
		this.root.appendChild(this.folderGui.removeDialog.container);
		this.root.appendChild(this.folderGui.progressDialog.container);
		this.root.appendChild(this.fileGui.renameDialog.container);
		this.root.appendChild(this.fileGui.removeDialog.container);
		this.root.appendChild(this.fileGui.previewDialog.container);
		this.root.appendChild(this.fileGui.buttons);
		this.root.appendChild(this.parent.aceContainer);

		this.rootFolder.querySelector('.folder-container').appendChild(this.folderGui.buttons);

		this.form.appendChild(this.rootFolder);
		this.form.appendChild(this.parent.clipboard.container);
		this.form.appendChild(this.folderGui.input);
	}

	init() {
		fetch('api/get-media', {
			credentials: 'same-origin'
		})
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			console.log(data);
			Utils.renderDirTree(
				data.media,
				this.rootFolder,
				this.rootContent
			);
				
			Utils.checkPublished(
				data.published,
				this.rootFolder,
				this.rootContent
			);

			if(localStorage.getItem('path')) {
				const path = localStorage.getItem('path').split('/').reverse();
				path.pop();
				this.rootFolder.querySelector('.folder-name').click();
				Utils.followPath(path);
			}
		});
	}
}

export class Users {
	constructor(parent) {
		this.root = document.querySelector('BODY');
		this.form = Utils.createElement('FORM');
		this.parent = parent;
		this.settings = new Types.UserSettings();

		this.settings.userSelect.addEventListener('change', (e) => {
			const userName = this.settings.userSelect.value;
			this.settings.name.value = '';
			this.settings.password.value = '';
			this.settings.newPassword.value = '';
			if(userName === 'New User') {
				this.settings.createUser = true;
				this.settings.newPassword.classList.add('hide');
				this.settings.newPasswordLabel.classList.add('hide');
				Array.from(this.settings.groupsContainer.querySelectorAll('input')).forEach(group => {
					group.checked = false;
				})
			} else {
				const user = this.settings.users[userName]; 
				this.settings.createUser = false;
				this.settings.newPassword.classList.remove('hide');
				this.settings.newPasswordLabel.classList.remove('hide');
				this.settings.name.value = this.settings.userSelect.value;
				Array.from(this.settings.groupsContainer.querySelectorAll('input')).forEach(group => {
					if(user.groups.includes(group.id))
						group.checked = true;
					else
						group.checked = false;
				})
			}
		});

		this.form.appendChild(this.settings.container);
	}	

	init() {
		fetch('api/get-users', {
			credentials: 'same-origin'
		})
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			this.settings.users = data;
			this.settings.userSelect.innerHTML = '';
			const addUser = Utils.createElement('OPTION', 'option-user');
			addUser.text = 'New User';
			this.settings.userSelect.appendChild(addUser);

			Object.keys(data).map((id) => {
				const userData = data[id];
				const userOption = Utils.createElement('OPTION', 'option-user');
				userOption.text = id;
				this.settings.userSelect.appendChild(userOption);
			})
		});
	}
}

export class Playlist {
	constructor(parent) {
		this.parent = parent;
		this.container = Utils.createElement('DIV');
	}

	init() {
		this.container.innerHTML = '';
		mediaplayer(
			this.container, 
			'get-media', 
			'static/private/' + user + '/',
			'mediaplayer'
		);
	}
}
