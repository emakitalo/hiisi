const handleStyle = (path) => {
	const head = document.querySelector('head');
	const font = document.createElement('LINK');
	const css = document.createElement('LINK');

	font.href = 'https://fonts.googleapis.com/css?family=Electrolize';
	font.rel = 'stylesheet';
	head.appendChild(font);
	css.rel = 'stylesheet';
	css.type = 'text/css';
	css.href = path + '/main.css';
	head.appendChild(css);
};

const createPlayer = () => {
	const info = document.createElement('DIV');
	const player = document.createElement('AUDIO');
	const playPause = document.createElement('DIV');
	const prev = document.createElement('DIV');
	const next = document.createElement('DIV');
	const source = document.createElement('SOURCE');
	const container = document.createElement('DIV');
	playPause.classList.add('player-play');
	playPause.innerHTML = '"';
	playPause.addEventListener('click', (e) => {
		const self = e.target;
		const audio = self.parentNode.querySelector('audio');
		if(!audio.paused) {
			self.innerHTML = '>';
			audio.pause();
		} else {
			self.innerHTML = '"';
			audio.play();
		}
	});
	prev.classList.add('player-prev');
	prev.innerHTML = '<<';
	prev.addEventListener('click', (e) => {
		const self = e.target;
		const player = self.parentNode;
		const audio = player.querySelector('audio');
		if(audio.src !== '') {
			const track = document.querySelector('li[path="' + audio.getAttribute('src') + '"]');
			const prev = track.previousSibling.classList.contains('item') ?
				track.previousSibling : track.parentNode.lastChild;
			audio.src = prev.getAttribute('path');
			audio.load();
			audio.play();
			player.querySelector('.info').innerHTML = 'Playing : ' + prev.innerHTML;
		}
	});
	next.classList.add('player-next');
	next.innerHTML = '>>';
	next.addEventListener('click', (e) => {
		const self = e.target;
		const player = self.parentNode;
		const audio = player.querySelector('audio');
		if(audio.src !== '') {
			const track = document.querySelector('li[path="' + audio.getAttribute('src') + '"]');
			const next = track.nextSibling ?
				track.nextSibling : track.parentNode.querySelector('.item');
			audio.src = next.getAttribute('path');
			audio.load();
			audio.play();
			player.querySelector('.info').innerHTML = 'Playing : ' + next.innerHTML;
		}
	});
	info.innerHTML = 'Track undefined';
	info.classList.add('info');
	player.classList.add('player');
	player.appendChild(source);
	container.classList.add('playerContainer');
	container.appendChild(info);
	container.appendChild(prev);
	container.appendChild(playPause);
	container.appendChild(next);
	container.appendChild(player);
	source.type = 'audio/mp3';
	return container; 
};

function recurse(data, path, obj) {
	const folder = path.pop();
	data.map(value => {
		if(typeof value === 'object') {
			if(value[folder]) {
				obj = value[folder];
				recurse(value[folder], path, obj);
			}
		}
	});
	return obj;
}

const getData = (context, url, stat, path) => {
	fetch(url, {
		method: "GET",
	}).then((re) => { 
		return re.json();
	}).then((data) => {
		console.log(data);
		const folders = path.split('/');
		console.log(folders);
		const audio = recurse(data.media, folders, []);
		console.log(audio);
		context.appendChild(
			handleData(
				audio,
				stat + path,
				document.createElement('DIV')
			)
		);
	});
};

const handleData = (data, path, list) => {
	const extensions = ['mp3', 'ogg', 'wav'];
	data.sort();
	data.map((content) => {
		switch(Object.prototype.toString.call(content)) {
			case '[object Object]':
				const key = Object.keys(content)[0];
				const newPath = path + '/' + key;
				const avatar = new Image(256, 256);
				avatar.src = newPath + '/' + 'cover.png';
				const subList = document.createElement('UL');
				const subListTitle = document.createElement('LI');
				subList.classList.add(key);
				subList.appendChild(avatar);
				subList.appendChild(subListTitle);
				subListTitle.innerHTML = key; 
				subListTitle.classList.add('subListTitle');
				handleData(content[key], newPath, list.appendChild(subList));
				break;
			case '[object String]':
				const song = content.split('.');
				if(extensions.includes(song.pop())) {
					const item = document.createElement('LI');
					list.appendChild(item);
					item.innerHTML = song.pop().split('_').join(' ').replace(/\b\w/g, l => l.toUpperCase());
					item.setAttribute('path', path + '/' + content);
					item.classList.add('item');
					item.addEventListener('click', (e) => {
						e.preventDefault();
						const self = e.target;
						const player = document.querySelector('.playerContainer');
						player.removeChild(player.querySelector('audio'));
						let audio = new Audio(self.getAttribute('path'));
						audio.addEventListener('ended', (e) =>{
							const self = e.target;
							const parent = item.parentNode;
							const next = parent.querySelector('li[path="' + self.getAttribute('src') + '"]').nextSibling;
							console.log('NEXT: ', next);
							console.log('NEXTI: ', parent.firstChild.nextSibling.nextSibling);
							if(next) {
								self.src = next.getAttribute('path'); 
								document.querySelector('.info').innerHTML = 'Playing : ' + next.innerHTML;
							} else {
								self.src = parent.firstChild.nextSibling.nextSibling.getAttribute('path');
								document.querySelector('.info').innerHTML = 'Playing : ' + parent.firstChild.nextSibling.nextSibling.innerHTML;
							}
							self.load();
							self.play();
						})
						audio.play();
						player.appendChild(audio);
						document.querySelector('.info').innerHTML = 'Playing : ' + self.innerHTML;
					});
				}
				break;
		}
	});
	return list;
};

export const init = (context, url, stat, path) => {
	handleStyle(stat + path);
	context.appendChild(createPlayer());
	getData(context, url, stat, path);
};

