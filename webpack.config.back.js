const path = require('path');
const nodemonPlugin = require('nodemon-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const devMode = process.env.NODE_ENV !== 'production'

const back = {
	target: "node",
  entry: {
		app: ["./src/back/main.js"]
	},
  output: {
		path: path.resolve(__dirname, "./dist/static/content/js"),
    filename: "back.bundle.js"
  },
	externals: [nodeExternals()],
	plugins: [
		new nodemonPlugin(),
	]
};

module.exports = [back];
