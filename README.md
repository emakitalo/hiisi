# TinyCMS

Content management system. Created with Webpack/Express/SASS/PassportJS.  

## Installation

Requirements  
  - [RethinkDB](https://rethinkdb.com/)
  - [Redis](https://redis.io/)

Install

```
export TINYCMS_USER="user_name"
export TINYCMS_PASSWORD = "user_password"
export REDIS_STORE_URI="redis_password"
npm install  
npm run build
node dist/static/js/back.bundle.js  
```

Go to address localhost://login and login with user_name and user_password